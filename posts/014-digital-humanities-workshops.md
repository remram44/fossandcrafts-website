title: 14: Digital Humanities Workshops
date: 2020-10-31 14:30
tags: episode, foss, digital humanities, racket, scribble
summary: Morgan and Chris discuss the Digital Humanities workshops they ran introducing non-programmers to Racket and Scribble
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/014-dhworkshops.ogg" length:"41486902" duration:"00:49:46"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/014-dhworkshops.mp3" length:"47788154" duration:"00:49:46"
---
Morgan and Chris discuss the
[Digital Humanities workshops](https://dustycloud.org/misc/digital-humanities/)
they ran introducing non-programmers to
[Racket](https://racket-lang.org/) and
[Scribble](https://docs.racket-lang.org/scribble/index.html).

![Participants' snowmen entries](/static/images/blog/Snowmen.png)

**Links:**

 - [The flier](https://dustycloud.org/tmp/lp2018-digital-humanities-flier.pdf)
   (post-LibrePlanet 2018 edition)
 - [Course material](https://dustycloud.org/misc/digital-humanities/)
   - [Building a Snowman with Racket](https://dustycloud.org/misc/digital-humanities/Snowman.html)
   - [How to Use Scribble to Write your Academic Papers: A Reference Tutorial](https://dustycloud.org/misc/digital-humanities/HowTo.html)
