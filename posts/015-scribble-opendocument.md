title: 15: Scribble and the Open Document Format
date: 2020-11-05 21:30
tags: episode, foss, digital humanities, racket, scribble, opendocument, odf, odt
summary: Morgan talks about writing her dissertation in Scribble while Chris talks about writing a Scribble exporter for the OpenDocument Format
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/015-scribble-opendocument.ogg" length:"45148446" duration:"00:53:31"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/015-scribble-opendocument.mp3" length:"51382612" duration:"00:53:31"
---
Morgan and Chris talk about the
[Scribble](https://docs.racket-lang.org/scribble/index.html)
document authoring format, with Morgan talking about authoring her
dissertation in it and Chris talking about writing an
[OpenDocument Format](http://opendocumentformat.org/) (sometimes
shortened to "ODF" or "ODT") exporter.
(That code is now a [merge request](https://github.com/racket/scribble/pull/284)
which will hopefully become part of Scribble itself!)

**Links:**

 - The [Digital Humanities Workshops](https://fossandcrafts.org/episodes/14-digital-humanities-workshops.html)
   episode... this is kind of a continuation of those topics.

 - [Racket](https://racket-lang.org/) and [Scribble](https://docs.racket-lang.org/scribble/index.html)

 - [Racket 7.4 release notes](https://download.racket-lang.org/v7.4.html),
   where Morgan is mentioned as a contributor.

*Clarification:* At one point we talk about whether or not Scribble
includes support for "image lists".  It has the relevant building
blocks with support for images and figures, we were talking a bit more
specifically about fitting a particular document formatting and
organizational pattern used in art history papers.
