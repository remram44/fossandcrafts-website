title: 7: [Theatre] What Escaped from the Demonic Z.O.O.O.O. (part 2)
date: 2020-08-27 15:45
tags: episode, theatre, rpg, freeform univeral
slug: 7-demonic-zoooo-part-2
summary: The conclusion of a live narrative RPG recording telling the story of several demonic office workers forced to deal with the escape of a dangerous kind of creature
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/007-what-escaped-from-demonic-zoooo-pt2.ogg" length:"63171853" duration:"01:43:24"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/007-what-escaped-from-demonic-zoooo-pt2.mp3" length:"99269884" duration:"01:43:24"
---
Leaving off from
[part 1](https://fossandcrafts.org/episodes/6-demonic-zoooo-part-1.html),
the office demons of Styx, Hexia, and Gummy Bear (or is it
Gerumphy or perhaps Gzeumphi Behr?) track down and confront the
mysterious lampmorel creature directly.  What dangers await them?
And just what secrets are the corporate overlords of Demonstrative
Industries and Plentimint Industries both keeping (perhaps even
from each other)?  Find out in this thrilling conclusion!
