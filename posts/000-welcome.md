title: Episode 0: Welcome to FOSS and Crafts!
date: 2020-07-15 10:30
tags: episode
summary: Introducing FOSS and Crafts
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/000-welcome.ogg" length:"20821813" duration:"00:23:33"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/000-welcome.mp3" length:"26195244" duration:"00:23:33"
---
Here it is, the very first episode of FOSS and Crafts!
Co-hosts Chris and Morgan introduce themselves, their backgrounds,
and give some sense of what to expect from the show.

Links:
- [Wikipedia summary of the Arts and Crafts movement](https://en.wikipedia.org/wiki/Arts_and_Crafts_movement)
