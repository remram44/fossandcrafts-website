title: 10: [Theatre] The What Goblins Saga, Chapter 1: What Are Goblins?
date: 2020-09-24 13:30
tags: episode, theatre, rpg, freeform universal, what goblins
slug: 10-what-goblins-ch1-what-are-goblins
summary: Live narrative RPG which asks the question, "What Are Goblins?"
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/010-what-goblins-ch1-what-are-goblins.ogg" length:"61874354" duration:"01:38:20"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/010-what-goblins-ch1-what-are-goblins.mp3" length:"94416131" duration:"01:38:20"
---
On this episode of FOSS and Crafts Theatre, we begin exploring
"The What Goblins Saga".
While the claim of
["our goblins are different"](https://tvtropes.org/pmwiki/pmwiki.php/Main/OurGoblinsAreDifferent)
is hardly new, these goblins seem to stand apart more than most...
even their fellow goblins seem to think so.
What is the nature of goblins, and what about The What Goblins in
particular?
Through little planning or foresight, our motley crew is about to find
more answers than they expected... which only opens up more questions,
of course...

**Links:**

 - [FOSS & Crafts Episode 1: Collabortive Storytelling with Dice](https://fossandcrafts.org/episodes/1-collaborative-storytelling-with-dice.html)
   introduces the idea of RPGs as a way of making narratives together.

 - See also [Freeform Universal](http://freeformuniversal.com/) (the
   RPG system used for this episode, explained in depth in Episode 1)!
