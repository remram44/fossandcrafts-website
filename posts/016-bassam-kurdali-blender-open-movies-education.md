title: 16: Bassam Kurdali on using Blender for open movie productions and education
date: 2020-11-12 14:50
tags: episode, interview, blender, foss, art, education, academia
slug: 16-bassam-kurdali-blender-open-movies-education
summary: Bassam Kurdali talks about using Blender (3d artwork software) for open Movie productions and education
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/016-bassam-kurdali-blender-open-movies-education.ogg" length:"29635247" duration:"00:47:46"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/016-bassam-kurdali-blender-open-movies-education.mp3" length:"45863047" duration:"00:47:46"
---
[Bassam Kurdali](https://urchn.org/)
([Fediverse](https://mastodon.social/@bkurdali), [Twitter](https://twitter.com/bkurdali))
talks about using [Blender](https://www.blender.org/)
(a free and open source software suite for making 3d artwork)
for open movie projects such as [Elephants Dream](https://orange.blender.org/)
(the world's first open movie project, which Bassam directed!)
and [Wires for Empathy](https://wiresforempathy.org/),
as well as use in teaching it to college students studying animation.

**Links:**

 - [Blender](https://www.blender.org/)
 - [Urchin studios](https://urchn.org/)
 - Chicken Chair (we need a better link for this... check back later!)
 - [Elephants Dream](https://orange.blender.org/)
 - [Wires for Empathy](https://wiresforempathy.org/) (aka "Tube")
 - [OpenToonz](https://opentoonz.github.io/e/)
 - [Boats Animator](https://www.charlielee.uk/boats-animator/)
 - [Natron](https://natrongithub.github.io/)
 - [Hampshire College](https://www.hampshire.edu/)
 - [Rhode Island School of Design (RISD)](https://www.risd.edu/)
 - [Blender splash screens gallery](https://cloud.blender.org/p/gallery/57e507b80fcf29412d1f1e53)
