title: 9: What is Spritely?
date: 2020-09-10 13:30
tags: episode, spritely, foss
slug: 9-what-is-spritely
summary: Chris and Morgan talk about what Spritely is as they run an errand
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/009-what-is-spritely.ogg" length:"41442896" duration:"00:50:05"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/009-what-is-spritely.mp3" length:"48091990" duration:"00:50:05"
---
What is this [Spritely](https://gitlab.com/spritely) project that's
taken up most of Chris's time for the last several years?
Something about advancing distributed/decentralized social networks,
but what does that mean?
Chris and Morgan talk about it while they drive to the bank!

![Steering wheel with episode outline taped to it](/static/images/blog/steering_wheel-spritely_episode.jpg)

**Links:**

 - [Spritely](https://gitlab.com/spritely)

   - [Spritely Goblins](https://docs.racket-lang.org/goblins/index.html) ([repo](https://gitlab.com/spritely/goblins)), the
     distributed programming language environment

   - The distributed storage demos: [Magenc](https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org), [Crystal](https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org), and [Golem](https://gitlab.com/spritely/golem/blob/master/README.org).  Currently being explored as the [Datashards](https://datashards.net/) project.

   - Where's the website?  Not up at the time of episode release yet.
     Get on that, Chris!
     
 - The [E programming language](http://www.erights.org/)

 - Rare video demonstrating [Electric Communities Habitat](https://www.youtube.com/watch?v=KNiePoNiyvE)

 - [CapTP](http://erights.org/elib/distrib/captp/index.html)

 - Goblins demos

   - [Terminal Phase time travel demo](https://dustycloud.org/blog/goblins-time-travel-micropreview/)

   - A [discussion](https://octodon.social/@cwebber/104825484376464543) about the
     [distributed chat demo](https://dustycloud.org/misc/goblins-chat-captp-onion-services.gif)
     mentioned in the episode

 - Interview where Chris [first announced Spritely](https://medium.com/we-distribute/faces-of-the-federation-christopher-allan-webber-on-mediagoblin-and-activitypub-24bbe212867e) on [We Distribute](https://medium.com/we-distribute)

 - [Spritely IRC channel](https://webchat.freenode.net/?channels=fossandcrafts)
