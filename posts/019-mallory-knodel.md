title: 19: Mallory Knodel on bits and bytes and human rights
date: 2020-12-17 15:50
tags: episode, guest, human rights, social justice, foss, standards
slug: 19-mallory-knodel
summary: Mallory Knodel joins us to talk about how social justice work is impacted by technical issues today, as well as the shared history between defining human rights and defining standards.
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/019-mallory-knodel.ogg" length:"42227666" duration:"01:09:37"
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/019-mallory-knodel.mp3" length:"66835578" duration:"01:09:37"
---
With computing technology becoming integrated with every aspect of our
lives, many issues are simultaneously human rights issues and
technical issues.
Thus, how are organizations concerned with human rights and social
justice engaging with technological authorship and policy-making?
[Mallory Knodel](https://twitter.com/MalloryKnodel), presently
[Chief Technology Officer](https://cdt.org/staff/mallory-knodel/)
for the [Center for Democracy and Technology](https://cdt.org/),
explains her work as a Public Interest Technologist.
Mallory is also heavily engaged in a wide number of technical
standards-making organizations, and explains not only how technical
standards are of interest to human rights organizations, but how
the origin in work to define human rights overlaps with the emergence
of standards-making efforts.

**Links:**

 - [Center for Democracy and Technology](https://cdt.org/)

 - The [Human Rights Protocol Considerations Research Group](https://datatracker.ietf.org/rg/hrpc/about/) (of which Mallory is co-chair)

 - A whole bunch of mentioned standards-making organizations:
   [W3C](https://www.w3.org/), [IETF](https://ietf.org/),
   [IRTF](https://irtf.org/), [IEEE](https://www.ieee.org/),
   [ITU](https://www.itu.int), [ICANN](https://www.icann.org/),
   [ISO](ISO)

 - [http://manu.sporny.org/2016/rebalancing/](Rebalancing How the Web is Built) by Manu Sporny

 - The slew of Google-related stuff, both good and bad:

   - [Google's attempt to control which browsers are permitted to use their services](https://developers.googleblog.com/2020/08/guidance-for-our-effort-to-block-less-secure-browser-and-apps.html)

   - [Controvercy around Google's AMP technology](https://themarkup.org/google-the-giant/2020/11/19/as-antitrust-pressure-mounts-google-to-pull-back-benefit-to-news-sites-that-adopted-its-preferred-mobile-technology)

   - [Google Is Testing End-to-End Encryption in Android Messages](https://www.wired.com/story/google-is-testing-end-to-end-encryption-in-android-messages/)

   - [3d party tracking cookies](https://www.bloomberg.com/news/articles/2020-11-23/google-should-be-u-k-antitrust-priority-online-marketers-warn)

 - [Governmental battles over root DNS split](https://www.theregister.com/2017/12/01/russia_own_internet/)

 - [Russia blocking TLS 1.3](https://www.zdnet.com/article/russia-wants-to-ban-the-use-of-secure-protocols-such-as-tls-1-3-doh-dot-esni/)

 - [IETF RFC 8890: The Internet is for End Users](https://tools.ietf.org/html/rfc8890)
