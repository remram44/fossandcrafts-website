title: 18: Sumana Harihareswara on sketching, standup, and maintainership
date: 2020-12-06 10:10
tags: episode, guest, standup, sketching, maintainership, foss, crafts
slug: 18-sumana-harihareswara
summary: Sumana Harihareswara talks about her experiences with sketching, standup, and maintainership and the idea of learning skills in "no big deal" contexts
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/018-sumana-harihareswara.ogg" length:"44276300" duration:"01:15:01"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/018-sumana-harihareswara.mp3" length:"72029575" duration:"01:15:01"
---
We're joined by [Sumana Harihareswara](https://www.harihareswara.net),
a FOSS advocate yes, but also a person of so many other talents!
We talk about sketching, standup comedy, and maintainership for the
long life of free software projects.
(Did you know you can hire Sumana to help on your FOSS project
maintainership btw?
Sumana runs [Changeset Consulting](https://changeset.nyc/)!)
We also talk about representation in the FOSS community within the arts
(especially narrative arts), and about learning new skills within
"no big deal" contexts.

**Links:**

 - [Changeset Consulting](https://changeset.nyc/)

 - Sumana's LibrePlanet 2017 keynote:
   [Lessons, myths, and lenses: What I wish I'd known in 1998](https://media.libreplanet.org/u/libreplanet/m/lessons-myths-and-lenses-what-i-wish-i-d-known-in-1998/)

 - [Producing Open Source Software](https://producingoss.com/)

 - [Vidding](https://fanlore.org/wiki/Vidding) and some of its origins in the
   [slideshow form](https://fanlore.org/wiki/Slideshow)
   (in particular with [Kandy Fong's](https://fanlore.org/wiki/Kandy_Fong)
   early works)

 - More on fanworks and fan communities and their history at [fanlore.org](https://fanlore.org)

 - [Vid: Pipeline](https://criticalcommons.org/Members/brainwane/clips/pipeline) by,
   as it turns out, [Sumana Harihareswara](https://www.harihareswara.net)!

 - [Vid: Only a Lad](https://archiveofourown.org/works/548124) by Laura Shapiro

 - [Vid: Straightening Up the House](https://eruthros.dreamwidth.org/343901.html) by eruthros; also see [all this other great commentary](https://eruthros.dreamwidth.org/344480.html)!
 
 - [The Bug](https://www.goodreads.com/book/show/138805.The_Bug) by Ellen Ullman
 
 - [Halt and Catch Fire](https://en.wikipedia.org/wiki/Halt_and_Catch_Fire_(TV_series))

 - [The Internet's Own Boy](https://en.wikipedia.org/wiki/The_Internet%27s_Own_Boy),
   a film and play about [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz),
   which you can [watch here in movie form](https://www.youtube.com/watch?v=9vz06QO3UkQ)
   (we're trying to find references to the play version... if you know
   something we should put up, let us know here!)

 - [Steven Levy's Hackers: Heroes of the Computer Revolution](https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution), and also the critical response [Programming is Forgetting: Toward a New Hacker Ethic](http://opentranscripts.org/transcript/programming-forgetting-new-hacker-ethic/) by Allison Parrish

 - [XKCD](https://xkcd.com/)

 - [Julia Styles in Ghostwriter](https://www.youtube.com/watch?v=bLlj_GeKniA)

 - [Software Freedom Conservancy](https://sfconservancy.org/), who is doing a
   [fundraiser right now](https://sfconservancy.org/supporter/)!

   - [Sumana's fundraising vid for Conservancy in 2015](https://sfconservancy.org/blog/2015/dec/22/sumana-2015/)

   - [Chris's animated ascii art card for Conservancy in 2019](https://dustycloud.org/blog/conservancy-card/)
     ([source code](https://gitlab.com/dustyweb/conservancy-postcard))

 - If you're interested in Sumana's upcoming book on long-term maintenance of
   FOSS projects, you can [contact her](https://changeset.nyc/#contact)
   for more info!
