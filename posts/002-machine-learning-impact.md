title: 2: The impact of machines that "learn" and produce
date: 2020-07-23 12:10
tags: episode
slug: 2-machine-learning-impact
summary: An analysis of the impact of recent machine learning developments on FOSS, art production, humanities research, and civic society
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/002-machine-learning-impact.ogg" length:"23309169" duration:"00:34:39"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/002-machine-learning-impact.mp3" length:"33270777" duration:"00:34:39"
---
The results from machine learning have been getting better and better
and the results seen so far from [OpenAI](https://openai.com)'s
[GPT-3 model](https://en.wikipedia.org/wiki/OpenAI#GPT-3) look stunningly
good.  But unlike [GPT-2](https://github.com/openai/gpt-2) (which was
publicly released under a free license), so far
[GPT-3 is accessible via API-only](https://openai.com/blog/openai-api/).
What's the reasoning and possible impact of that decision?
For that matter, what kind of impacts could machine learning advancements
make on FOSS, programming in general, art production, and civic society?

**Links:**

 - The [OpenAI's GPT-3 may be the biggest thing since bitcoin](https://maraoz.com/2020/07/18/openai-gpt3/) article
   - The [quoted comment on Hacker News](https://news.ycombinator.com/item?id=23886503)
 - [Auto-generation of legalese](https://twitter.com/edleonklinger/status/1284251420544372737) and
   [auto-web-design](https://twitter.com/sharifshameem/status/1282676454690451457) GPT-3 demos
 - [GPT-2](https://github.com/openai/gpt-2)
 - [GPT-3's API and FAQ page](https://openai.com/blog/openai-api/)
 - [Tensorflow](https://www.tensorflow.org/) and [PyTorch](https://pytorch.org/)
 - [(Artificial) neural networks](https://en.wikipedia.org/wiki/Artificial_neural_networks) and [machine learning](https://en.wikipedia.org/wiki/Machine_learning)
 - [https://thispersondoesnotexist.com/](https://thispersondoesnotexist.com/)
 - Google's [Deepmind](https://deepmind.com/) and [Agent57](https://deepmind.com/blog/article/Agent57-Outperforming-the-human-Atari-benchmark) (be sure to watch the [Agent57 videos](https://www.youtube.com/watch?list=PLqYmG7hTraZCHS3JLle_kxwNvImpYVq4z&time_continue=1&v=luZm3jmwGwI&feature=emb_logo), they're's impressive)
 - Mozilla's [Common Voice](https://voice.mozilla.org/) project
 - [AlphaGo](https://deepmind.com/research/case-studies/alphago-the-story-so-far)
 - [Bayesian spam filters](https://en.wikipedia.org/wiki/Naive_Bayes_spam_filtering);
   see also Paul Graham's highly influential
   [a plan for spam](http://paulgraham.com/spam.html) writeup
 - [Markov chains](https://en.wikipedia.org/wiki/Markov_chain)
   (we miss you, [X11R5](https://www.x11r5.com/)...)
 - The [Postmodernist Essay Generator](http://www.elsewhere.org/journal/pomo/)
 - [Postmodernism](https://en.wikipedia.org/wiki/Postmodernism)
 - Neural networks' difficulties in explaining "why they did that"
   and an overview of attempts to make things better: [An Overview of Interpretability of Machine Learning](https://arxiv.org/pdf/1806.00069)
 - Chris had a
   [conversation with Gerald Sussman about AI that was related to the above and influential on them](https://dustycloud.org/blog/sussman-on-ai/).
   "If an AI driven car drives off the side of the road, I want to
   know why it did that. I *could* take the software developer to
   court, but I would much rather take the AI to court."
 - The Propagator Model (by Alexey Radul and Gerald Jay Sussman, largely):
   [Revised Report on the Propagator Model](https://groups.csail.mit.edu/mac/users/gjs/propagators/).
   See also: [We Really Don't Know How to Compute!](https://www.youtube.com/watch?v=O3tVctB_VSU)
 - [Three Panel Soul](http://www.threepanelsoul.com/)'s
   [Recursion](http://www.threepanelsoul.com/comic/recursion) comic
   (cut from this episode, but we also
   originally mentioned their
   [Techics](http://www.threepanelsoul.com/comic/tethics) comic which
   is definitely relevant though)
 - [Surrealism](https://en.wikipedia.org/wiki/Surrealism),
   [Abstract Expressionism](https://en.wikipedia.org/wiki/Abstract_impressionism),
   [Impressionism](https://en.wikipedia.org/wiki/Impressionism),
   and the [Realism](https://en.wikipedia.org/wiki/Realism_(art_movement)) movement
   (Obviously there's also a lot more to say about these art movements than just
   lumping them as a reaction to photography but... only so much time on an
   episode.)
 - [AI Dungeon 2](https://play.aidungeon.io/) (nonfree, though you can play it in your browser)
 - Episode of [Ludology](https://ludology.libsyn.com/) about
   [procedural narrative generation](https://ludology.libsyn.com/gametek-classic-149-procedural-narrative-generation)
 - [Implicit Bias and the Teaching of Writing](https://www.bates.edu/writing/2018/03/12/implicit-bias-and-the-teaching-of-writing/)
 - Machine learning's tendency to inherit biases
   - [Rise of the racist robots -- how AI is learning all our worst impulses](https://www.theguardian.com/inequality/2017/aug/08/rise-of-the-racist-robots-how-ai-is-learning-all-our-worst-impulses)
   - [Machine bias](https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing)
     (and its use in deciding court cases)
   - [Google's Vision AI producing racist results](https://algorithmwatch.org/en/story/google-vision-racism/)
   - [When It Comes to Gorillas, Google Photos Remains Blind](https://www.wired.com/story/when-it-comes-to-gorillas-google-photos-remains-blind/)
     (Content warning: this is due to an extremely harmful form of synthesized
     racism from the biases in the datasets Google has used)
   - [Predictive policing algorithms are racist.  They need to be dismantled.](https://www.technologyreview.com/2020/07/17/1005396/predictive-policing-algorithms-racist-dismantled-machine-learning-bias-criminal-justice/)
 - [Discovering Reinforcement Learning Algorithms](https://arxiv.org/abs/2007.08794) and the subdiscipline of
   [Learning to Learn](https://en.wikipedia.org/wiki/Meta_learning_(computer_science))
 - [Hayo Miazaki's criticism of an AI demonstration not considering its impact](https://www.youtube.com/watch?v=BfxlgHBaxEU)
