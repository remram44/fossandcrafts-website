;;; This file is part of fossandcrafts.org, but it's the only piece
;;; released under GPLv3+, basically for compatibility reasons.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; To set up a hacking environment:
;;   guix environment -l guix.scm

(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages texinfo)
             (gnu packages pkg-config))

;; We're currently using some new updates to enable podcasting not yet
;; in the latest release
(define haunt-from-git
  (package
   (inherit haunt)
   (version "git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/dustyweb/haunt.git")
                  (commit "a6d6970ec998860650c5209cf57d90c8cb0e7421")))
            (sha256
             (base32
              "1zlf45kwrcv9pkfyi87zh93clba8csk7mf5m38bkbm2ga1rwh2dp"))))
   (arguments
    `(#:modules ((ice-9 match) (ice-9 ftw)
                 ,@%gnu-build-system-modules)
      #:tests? #f ; test suite is non-deterministic :(
      #:phases (modify-phases %standard-phases
                 ;; this one is custom
                 (add-before 'configure 'bootstrap
                   (lambda _
                     (zero? (system* "./bootstrap"))))
                 ;; This one came straight from haunt's definition
                 ;; in Guix
                 (add-after 'install 'wrap-haunt
                   (lambda* (#:key inputs outputs #:allow-other-keys)
                     ;; Wrap the 'haunt' command to refer to the right
                     ;; modules.
                     (let* ((out  (assoc-ref outputs "out"))
                            (bin  (string-append out "/bin"))
                            (site (string-append
                                   out "/share/guile/site"))
                            (guile-reader (assoc-ref inputs "guile-reader"))
                            (deps `(,@(if guile-reader
                                          (list guile-reader)
                                          '())
                                    ,(assoc-ref inputs "guile-commonmark"))))
                       (match (scandir site)
                         (("." ".." version)
                          (let ((modules (string-append site "/" version))
                                (compiled-modules (string-append
                                                   out "/lib/guile/" version
                                                   "/site-ccache")))
                            (wrap-program (string-append bin "/haunt")
                              `("GUILE_LOAD_PATH" ":" prefix
                                (,modules
                                 ,@(map (lambda (dep)
                                          (string-append dep
                                                         "/share/guile/site/"
                                                         version))
                                        deps)))
                              `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                                (,compiled-modules
                                 ,@(map (lambda (dep)
                                          (string-append dep "/lib/guile/"
                                                         version
                                                         "/site-ccache"))
                                        deps))))
                            #t)))))))))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)
      ,@(package-native-inputs haunt)))))

(package
  (name "fossandcrafts-website")
  (version "git")
  (source #f)
  (build-system gnu-build-system)
  (synopsis #f)
  (description #f)
  (license #f)
  (home-page #f)
  (inputs
   `(("guile" ,guile-2.2)))
  (propagated-inputs
   `(("haunt" ,haunt-from-git)
     ("guile-reader" ,guile-reader)
     ("guile-reader" ,guile-commonmark)
     ("guile-syntax-highlight" ,guile-syntax-highlight))))
